# MotherMachine

## License heads-up

While the code itself is my own creation and is available freely, the textures were created with a **non-commercial** version of Substance Painter and the sound effects are licensed as **non-commercial** as well.

## Controls

* ``WSAD`` – movement
* ``Shift`` – sprint
* ``Space`` – jump
* ``Ctrl`` – crouch
* ``LMB`` – shoot
* ``RMB`` – aim
* ``Q`` – use healing item
* ``E`` – toggle firing mode between **Drain** and **Charge**
* ``Alt + F4`` – close game

## Gun modes

* Drain – blue icon, purple canister
* Charge – red icon, red canister

## Gameplay

* Attack enemies with **Drain** mode to charge your gun with energy.
* Attack constructs (e.g. doors or health dispensers) with **Charge** mode to charge and activate them.
* Game is over when your health reaches zero.
* Weapon heats up as you're shooting. When heat reaches maximum, an indicator lights up and you need to wait for the bar to deplete completely before being able to shoot again.

## UI

### Status bars

![status bars](https://i.imgur.com/KAwdlb7.png)

* Health bar indicates health.
* Critical heat indicator lights up in red when your heat reaches maximum.
* Heat bar indicates your weapon overheating. Reaching maximum lights up critical heat indicator.
* Energy bar indicates how much energy you have stored.

### Mode indicator

![mode indicator](https://i.imgur.com/ygaeQKs.png)

* Blue icon being bigger and on top, indicates **Drain** mode enabled.
* Red icon being bigger and on top indicates **Charge** mode enabled.